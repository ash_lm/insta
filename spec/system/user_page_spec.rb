require 'rails_helper'

RSpec.describe "UserPage", type: :system do
  # 投稿
  let(:user) do
    create(:user, username: "pika") do |u|
      create_list(:post, 13, :with_image, user: u)
    end
  end
  # お気に入り
  let(:iine_user) do
    create(:user) do |u|
      create_list(:post, 11, :with_image, user: u)
    end
  end
  # フォロー
  let(:following_users) { create_list(:user, 14) }
  # フォロワー
  let(:follower_users) { create_list(:user, 12) }

  # 表示に必要なデータ生成
  before do
    iine_user.posts.each { |post| user.iine(post) }
    following_users.each { |following_user| user.follow(following_user) }
    follower_users.each { |follower_user| follower_user.follow(user) }
    sign_in user
  end

  # 各部チェック
  def expect_page_check(title, list)
    aggregate_failures do
      expect(page).to have_title title
      expect(page).to have_content list
      expect(page).to have_content "お気に入り 11"
      expect(page).to have_content "フォロー中 14"
      expect(page).to have_content "フォロワー 12"
      expect(page).to have_css(".pagination")
    end
  end

  describe "ユーザー関連のページが正しく表示されているかチェックする" do
    context "マイページ" do
      it "タイトル、各種件数とページネーションが正しく表示されていること" do
        visit user_path(user)
        expect_page_check("pika・Insta!", "投稿リスト 13 件")
        expect(all("img[src$='sample.jpg']").count).to eq 9
        # マイページの場合 編集ボタンが表示
        expect(page).to have_selector(".btn-info", text: "ユーザー編集")
        expect(page).not_to have_button "フォロー"
      end
    end

    context "お気に入りページ" do
      it "タイトル、各種件数とページネーションが正しく表示されていること" do
        visit iines_user_path(user)
        expect_page_check("お気に入り・Insta!", "お気に入りリスト 11 件")
      end
    end

    context "フォローページ" do
      it "タイトル、各種件数とページネーションが正しく表示されていること" do
        visit following_user_path(user)
        expect_page_check("フォロー・Insta!", "フォローリスト 14 件")
        expect(all("ol li").count).to eq 10
      end
    end

    context "フォロワーページ" do
      it "タイトル、各種件数とページネーションが正しく表示されていること" do
        visit followers_user_path(user)
        expect_page_check("フォロワー・Insta!", "フォロワーリスト 12 件")
        expect(all("ol li").count).to eq 10
      end
    end
  end
end
