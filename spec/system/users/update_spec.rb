require 'rails_helper'

RSpec.describe "Update", type: :system do
  include ActiveJob::TestHelper
  let(:user) { create(:user) }

  after do
    ActionMailer::Base.deliveries.clear
  end

  before do
    sign_in user
  end

  describe "ユーザー情報の編集" do
    it "編集ページにてパスワード以外の情報を正常に編集させる" do
      # マイページから編集ページへアクセス
      visit user_path(user)
      click_on "ユーザー編集"
      expect(current_path).to eq edit_user_registration_path
      expect(page).to have_title "アカウント情報編集・Insta!"

      # 一旦、キャンセルボタンを押してマイページへ戻る
      click_on "キャンセルして戻る"
      expect(current_path).to eq user_path(user)
      click_on "ユーザー編集"

      # 更新に失敗
      fill_in "氏名", with: nil
      click_on "更新する"
      expect(page).to have_css("#error_explanation")

      # 各種情報を入力して更新させる
      fill_in "氏名", with: "mimikkyu"
      fill_in "ユーザーネーム", with: "mimi"
      choose "男性"
      fill_in "携帯番号", with: "090-1234-5678"
      fill_in "プロフィール", with: "あいうえお"
      fill_in "URL", with: "http://mimikkyu/example.com"
      attach_file "user_avatar", "#{Rails.root}/spec/files/sample.jpg"
      click_on "更新する"

      aggregate_failures do
        expect(user.reload.fullname).to eq "mimikkyu"
        expect(user.reload.username).to eq "mimi"
        expect(user.reload.gender).to eq "male"
        expect(user.reload.profile).to eq "あいうえお"
        expect(user.reload.url).to eq "http://mimikkyu/example.com"
        expect(user.avatar.attached?).to eq true
        expect(page).to have_selector(".alert-success", text: "アカウント情報を変更しました。")
        expect(current_path).to eq user_path(user)
      end
    end
  end

  describe "メールアドレスの編集" do
    it "入力送信後に確認メールを通じて変更を完了させる" do
      perform_enqueued_jobs do
        # アドレスを編集すると認証メールが送信される
        visit edit_user_registration_path
        fill_in "メールアドレス", with: "mimi@example.com"
        expect { click_on "更新する" }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)

        # 認証メールのリンクを通じて編集内容を有効にする
        token = user.reload.confirmation_token
        visit(user_confirmation_path(confirmation_token: token))
        expect(page).to have_content "メールアドレスが確認できました。"
        expect(user.reload.email).to eq "mimi@example.com"
      end
    end
  end

  describe "パスワード情報の編集" do
    it "パスワード編集ページにてパスワードのみ正常に編集させる" do
      # 編集ページからパスワード編集ページにアクセス
      visit edit_user_registration_path
      click_on "パスワード変更"
      expect(current_path).to eq users_edit_password_path
      expect(page).to have_title "パスワード変更・Insta!"

      # 一旦、キャンセルして編集ページへ戻る
      click_on "キャンセルして戻る"
      expect(current_path).to eq edit_user_registration_path

      # 新しいパスワードに変更する
      # 変更失敗
      click_on "パスワード変更"
      in_password("pass")
      expect(page).to have_css("#error_explanation")

      # 変更成功
      in_password("newpassword")
      aggregate_failures do
        expect(current_path).to eq user_path(user)
        expect(page).to have_selector(".alert-success", text: "パスワードを変更しました")
      end

      # 一度サインアウトをしてから、新しいパスワードでログインする
      click_link nil, href: destroy_user_session_path
      visit new_user_session_path

      # ヘッダーにはログアウトは表示されていない
      expect(page).not_to have_link nil, href: destroy_user_session_path
      fill_in "メールアドレス", with: user.email
      fill_in "パスワード", with: "newpassword"
      click_on "ログイン"

      # ログインをしたらヘッダーにログアウトが表示される
      expect(page).to have_selector(".alert-success", text: "ログインしました。")
      expect(page).to have_link nil, href: destroy_user_session_path
    end

    def in_password(password)
      fill_in "変更後のパスワード", with: password
      fill_in "変更後のパスワード（確認）", with: password
      fill_in "現在のパスワード", with: "password"
      click_on "更新する"
    end
  end

  describe "アカウントの削除" do
    it "ユーザー編集ページから削除を実行する" do
      visit edit_user_registration_path
      expect do
        page.accept_confirm("本当にアカウントを削除をしてよろしいですか?") do
          click_on("アカウントを削除する")
        end
        expect(page).to have_selector(".alert-success", text: "アカウントを削除しました。またのご利用をお待ちしております。")
        expect(current_path).to eq root_path
      end.to change(User, :count).by(-1)
    end
  end
end
