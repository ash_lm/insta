require 'rails_helper'

RSpec.describe "Sign_in", type: :system do
  let!(:user) { create(:user) }

  include ActiveJob::TestHelper
  describe "ログインとログアウトのチェック" do
    let(:non_email) { "pi@example.com" }
    let(:non_password) { "paasword" }
    let(:email) { user.email }
    let(:password) { user.password }

    it "ログイン状況でヘッダーの表示が切り替わること" do
      # 未ログインではヘッダーにログアウトが表示されない
      visit root_path
      expect(page).not_to have_link nil, href: destroy_user_session_path
      click_on "ログイン"
      expect(page).to have_title "ログイン・Insta!"

      # ログイン失敗
      failure_sign_in
      expect(page).to have_selector(".alert-danger", text: "メールアドレスまたはパスワードが違います。")

      # ログイン成功
      # ログイン状態ではヘッダーにログアウトが表示される
      success_sign_in
      aggregate_failures do
        expect(current_path).to eq user_path(user)
        expect(page).to have_selector(".alert-success", text: "ログインしました。")
        expect(page).to have_link nil, href: destroy_user_session_path
      end

      # ログアウト
      click_link nil, href: destroy_user_session_path
      aggregate_failures do
        expect(current_path).to eq root_path
        expect(page).to have_selector(".alert-success", text: "ログアウトしました。")
        expect(page).not_to have_link nil, href: destroy_user_session_path
      end
    end
  end

  describe "アカウント登録前のログイン" do
    let(:non_email) { "pi@example.com" }
    let(:non_password) { "paasword" }

    it "新規登録ページへリダイレクトすること" do
      visit new_user_session_path
      failure_sign_in
      expect(current_path).to eq new_user_session_path
      expect(page).to have_selector(".alert-danger", text: "メールアドレスまたはパスワードが違います。")
    end
  end

  describe "アカウント有効前のログイン" do
    let(:non_account) { create(:user, :confirmation) }
    let(:email) { non_account.email }
    let(:password) { non_account.password }

    it "有効にしなければログインできないこと" do
      visit new_user_session_path
      success_sign_in
      expect(current_path).to eq new_user_session_path
      expect(page).to have_selector(".alert-danger", text: "メールアドレスの本人確認が必要です。")
    end
  end

  describe "remember機能のチェック" do
    def login_remember
      visit new_user_session_path
      fill_in "メールアドレス", with: user.email
      remember
      fill_in "パスワード", with: user.password
      click_on "ログイン"
    end

    context "remember ON" do
      let(:remember) { check "ログインを記憶" }

      it "１時間経過してもタイムアウトしない" do
        perform_enqueued_jobs do
          login_remember
          travel_to 60.minutes.after do
            visit user_path(user)
            expect(page).to have_title user.username
            expect(user.reload.remember_created_at).not_to eq nil
          end
        end
      end
    end

    context "remember OFF" do
      let(:remember) { uncheck "ログインを記憶" }

      it "１時間経過でタイムアウトする" do
        perform_enqueued_jobs do
          login_remember

          travel_to 60.minutes.after do
            visit user_path(user)
            aggregate_failures do
              expect(user.reload.remember_created_at).to eq nil
              expect(page).not_to have_content user.fullname
              expect(page).to have_selector(".alert-danger", text: "ログインしてください。")
              expect(current_path).to eq new_user_session_path
            end
          end
        end
      end
    end
  end

  def success_sign_in
    fill_in "メールアドレス", with: email
    fill_in "パスワード", with: password
    click_on "ログイン"
  end

  def failure_sign_in
    fill_in "メールアドレス", with: non_email
    fill_in "パスワード", with: non_password
    click_on "ログイン"
  end
end
