require 'rails_helper'

RSpec.describe "Omniauthでログイン", js: true, type: :system do
  context "Facebookでログインする" do
    before do
      OmniAuth.config.mock_auth[:facebook] = nil
      Rails.application.env_config['omniauth.auth'] = set_omniauth
      visit root_path
    end

    it "サインアップに成功するとユーザーがカウントされる" do
      expect do
        click_on "Facebookでログイン"
        sleep 1
      end.to change(User, :count).by(1)

      # アカウント有効化
      user = User.last
      token = user.confirmation_token
      visit(user_confirmation_path(confirmation_token: token))
      expect(page).to have_content "メールアドレスが確認できました。"

      # 連携済みならカウントはされない
      expect do
        click_on "Facebook でログイン"
        sleep 1
      end.not_to change(User, :count)
      expect(current_path).to eq user_path(user)
    end
  end
end
