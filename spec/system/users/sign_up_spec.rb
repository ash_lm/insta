require 'rails_helper'

RSpec.describe "Sign_up", type: :system do
  include ActiveJob::TestHelper

  after do
    ActionMailer::Base.deliveries.clear
  end

  it "入力した後で確認画面を経て登録して、送信されたメールからアカウントを有効にする" do
    perform_enqueued_jobs do
      visit new_user_registration_path
      expect(page).to have_title "新規登録・Insta!"
      # 登録に失敗
      click_on "アカウントを作成する"
      expect(page).to have_content "氏名を入力してください"
      expect(page).to have_content "ユーザーネームを入力してください"
      expect(page).to have_content "メールアドレスを入力してください"
      expect(page).to have_content "パスワードを入力してください"

      # 登録に成功
      fill_in "氏名", with: "pikachu"
      fill_in "ユーザーネーム", with: "pika"
      fill_in "メールアドレス", with: "pika@example.com"
      fill_in "パスワード", with: "password"
      fill_in "確認用パスワード", with: "password"
      click_on "アカウントを作成する"

      # 確認画面で入力した内容が反映されているかチェック
      aggregate_failures do
        expect(page).to have_title "確認画面・Insta!"
        expect(current_path).to eq "/users/sign_up/confirm.user"
        expect(find("#user_fullname", visible: false).value).to eq "pikachu"
        expect(find("#user_username", visible: false).value).to eq "pika"
        expect(find("#user_email", visible: false).value).to eq "pika@example.com"
        expect(find("#user_password", visible: false).value).to eq "password"
      end

      # 戻るボタンで入力画面に戻りパスワードだけ入力して再度確認画面へ
      click_on "戻る"
      fill_in "パスワード", with: "password"
      fill_in "確認用パスワード", with: "password"
      click_on "アカウントを作成する"

      # 確定するとメール送信されてTopページにリダイレクトされる
      expect { click_on "確定する" }.to change { ActionMailer::Base.deliveries.size }.by(1)
      expect(current_path).to eq root_path
      expect(page).to have_content "アカウント確認のメールを送信しました。内容を確認してください。"

      # メール内容チェック
      mail = ActionMailer::Base.deliveries.last
      user = User.last
      aggregate_failures do
        expect(mail.from).to eq ["#{ENV['GMAIL_ADDRESS']}"]
        expect(mail.to).to eq [user.email]
        expect(mail.subject).to eq "メールアドレス確認メール"
        expect(mail.body).to include user.fullname
      end

      # アカウントを有効にする
      token = user.confirmation_token
      visit(user_confirmation_path(confirmation_token: token))
      expect(page).to have_title "ログイン・Insta!"
      expect(page).to have_content "メールアドレスが確認できました。"

      # もう一度送信するとエラーになる
      visit(user_confirmation_path(confirmation_token: token))
      expect(page).to have_content "メールアドレスは既に登録済みです。"
    end
  end

  describe "アカウント有効期限のチェック" do
    it "送信してから24時間経過で有効期限が切れる" do
      perform_enqueued_jobs do
        signup_info
        travel_to 25.hours.after do
          user = User.last
          token = user.confirmation_token
          visit(user_confirmation_path(confirmation_token: token))
          expect(page).to have_content "メールアドレスの期限が切れました。"
          expect(current_path).to eq user_confirmation_path
        end
      end
    end
  end

  describe "確認メールを再送信する" do
    it "再送信したメールを正常に受け取りアカウントを有効にする" do
      perform_enqueued_jobs do
        # メールを再送信する
        signup_info
        visit new_user_session_path
        click_on "アカウント確認メールが届きませんでしたか？"
        expect(page).to have_title "確認メール再送信・Insta!"

        fill_in "メールアドレス", with: "pika@example.com"
        expect { click_on "送信する" }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)

        # アカウントを有効にする
        user = User.last
        token = user.confirmation_token
        visit(user_confirmation_path(confirmation_token: token))
        expect(page).to have_content "メールアドレスが確認できました。"
      end
    end
  end

  def signup_info
    visit new_user_registration_path
    fill_in "氏名", with: "pikachu"
    fill_in "ユーザーネーム", with: "pika"
    fill_in "メールアドレス", with: "pika@example.com"
    fill_in "パスワード", with: "password"
    fill_in "確認用パスワード", with: "password"
    click_on "アカウントを作成する"
    click_on "確定する"
  end
end
