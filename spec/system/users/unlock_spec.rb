require 'rails_helper'

RSpec.describe "Unlock", type: :system do
  include ActiveJob::TestHelper

  let!(:user) { create(:user) }

  after do
    ActionMailer::Base.deliveries.clear
  end

  it "アカウントを凍結させて、案内メールを再送信させる" do
    perform_enqueued_jobs do
      # ３回目の失敗で警告される
      visit new_user_session_path
      fill_in "メールアドレス", with: user.email
      in_password("違うパスワード")
      in_password("違うパスワード")
      in_password("違うパスワード")
      expect_flash(".alert-danger", "もう一回誤るとアカウントがロックされます。")

      # 凍結するとメールが自動送信される
      expect { in_password("違うパスワード") }.to change {
        ActionMailer::Base.deliveries.size
      }.by(1)
      expect_flash(".alert-danger", "アカウントは凍結されています。")
      # 正しい情報を入力してもログインできない
      in_password(user.password)
      expect_flash(".alert-danger", "アカウントは凍結されています。")

      # 解除メールを再送信させる
      click_on "アカウントの凍結解除方法のメールを受け取っていませんか?"
      aggregate_failures do
        expect(current_path).to eq new_user_unlock_path
        expect(page).to have_title "解除メール再送信・Insta!"
        fill_in "メールアドレス", with: user.email
        expect { click_on "送信する" }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)
        expect_flash(".alert-success", "アカウント凍結解除メールを再送信しました")
      end

      # 凍結を解除する system specで成功パターン作成済み
      token = user.reload.unlock_token
      expect(token).not_to eq nil
      # テストだとトークンエラーで失敗する・・・・・・
      # visit user_unlock_path(unlock_token: token)
      # expect(page).to have_title "ログイン・Insta!"
      # expect(page).to have_content "アカウントを凍結解除しました。"
    end
  end

  def in_password(password)
    fill_in "パスワード", with: password
    click_on "ログイン"
  end

  def expect_flash(alert, messege)
    expect(page).to have_selector(alert, text: messege)
  end
end
