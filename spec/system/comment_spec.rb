require 'rails_helper'

RSpec.describe "Comment", js: true, type: :system do
  let(:user) { create(:user, :with_post) }

  describe "コメント投稿機能をチェックする" do
    # コメント投稿
    def expect_post_comment(comment)
      expect do
        fill_in "comment[comment]", with: comment
        within ".actions" do
          click_on "投稿する"
        end
        expect(page).to have_selector("p", text: comment)
      end.to change { user.comments.count }.by(1)
    end

    it "投稿したコメントが降順で表示されていること" do
      post = user.posts[0]
      sign_in user
      visit post_path(post)
      expect(page).to have_title "#{post.content}・Insta!"

      # コメントに失敗
      expect do
        fill_in "comment[comment]", with: nil
        within ".actions" do
          click_on "投稿する"
        end
        expect(current_path).to eq post_path(post)
      end.not_to change { user.comments.count }

      # コメントに成功した内容が降順で表示
      aggregate_failures do
        expect_post_comment("あいうえお")
        expect_post_comment("かきくけこ")
        expect_post_comment("さしすせそ")
        expect(first("ol .bl_comment_box")).to have_content("さしすせそ")
      end
    end
  end
end
