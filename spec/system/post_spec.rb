require 'rails_helper'

RSpec.describe "Post", js: true, type: :system do
  let(:user) { create(:user) }

  before do
    sign_in user
  end

  # 投稿削除
  def expect_post_delete
    expect do
      page.accept_confirm("投稿を削除をしてよろしいですか?") do
        click_on("削除する")
      end
      expect(current_path).to eq user_path(user)
      expect(page).to have_content "投稿を削除しました。"
    end.to change(Post, :count).by(-1)
  end

  it "投稿ページをから画像を投稿して削除する" do
    # 投稿ページへアクセス
    visit user_path(user)
    # 投稿がない場合に表示される
    expect(page).to have_selector("h1", text: "画像を投稿してみよう！")
    click_link "画像を投稿する"
    expect(page).to have_title "新規投稿・Insta!"
    click_on "キャンセルして戻る"
    expect(current_path).to eq user_path(user)

    # 投稿する
    click_link "画像を投稿する"
    sleep 1
    # 投稿に失敗
    within ".actions" do
      expect { click_on "投稿する" }.not_to change(Post, :count)
    end
    expect(page).to have_selector "#error_explanation"

    # 投稿に成功
    attach_file "post[image]", "#{Rails.root}/spec/files/sample.jpg", make_visible: true
    fill_in "post_content", with: "あいうえお"
    within ".actions" do
      expect { click_on "投稿する" }.to change(Post, :count).by(1)
    end
    # マイページに投稿内容が反映される
    aggregate_failures do
      expect(current_path).to eq user_path(user)
      expect(page).not_to have_selector("h1", text: "画像を投稿してみよう！")
      expect(page).to have_content "1 件"
      expect(page).to have_selector("img[src$='sample.jpg']")
    end

    # 自分の投稿ならお気に入りボタンの代わりに削除ボタンが表示
    find("img[src$='sample.jpg']").click
    aggregate_failures do
      expect(page).to have_button "コメントする"
      expect(page).to have_button "投稿を削除する"
      expect(page).not_to have_button "お気に入り"
    end

    # 削除する
    expect_post_delete
  end

  describe "投稿個別ページで投稿を削除する" do
    let(:user) { create(:user, :with_post) }

    it "正常に作動すること" do
      post = user.posts[0]

      # 自分の投稿なら削除ボタンが表示
      visit post_path(post)
      aggregate_failures do
        expect(page).to have_button "投稿を削除する"
        expect(page).not_to have_button "コメントする"
        expect(page).not_to have_button "お気に入り"
      end

      # 削除する
      expect_post_delete
    end
  end
end
