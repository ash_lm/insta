require 'rails_helper'

RSpec.describe "Follow", js: true, type: :system do
  let(:user) { create(:user) }
  let(:other_user) { create(:user, :with_post) }

  before do
    sign_in user
  end

  # フォロー機能
  def expect_follow_change
    # フォロー解除
    expect do
      click_on "フォロー中"
      sleep 1
    end.to change(user.following, :count).by(-1)
    # フォローする
    expect do
      click_on "フォローする"
      sleep 1
    end.to change(user.following, :count).by(1)
  end

  describe "各ページに配置したフォローボタンが機能するかチェックする" do
    context "マイページ" do
      it "正常にフォロー機能が作動すること" do
        visit user_path(other_user)
        # 他ユーザーのマイページなら編集ボタンの代わりにフォローボタンが表示
        expect(page).to have_button "フォローする"
        expect(page).not_to have_button "ユーザー編集"

        # フォローする カウントも同時に更新される
        expect do
          click_on "フォローする"
          expect(page).to have_content "フォロワー 1"
        end.to change(user.following, :count).by(1)

        # フォローを解除する
        expect do
          click_on "フォロー中"
          expect(page).to have_content "フォロワー 0"
        end.to change(user.following, :count).by(-1)
      end
    end

    context "投稿個別ページ" do
      before do
        user.follow(other_user)
      end

      it "正常にフォロー機能が作動すること" do
        post = other_user.posts[0]
        visit post_path(post)
        expect(page).to have_title "#{post.content}・Insta!"

        # フォロー機能チェック
        expect_follow_change
      end
    end

    context "トップページ" do
      before do
        user.follow(other_user)
      end

      it "正常にフォロー機能が作動すること" do
        visit root_path
        expect(page).to have_title "Insta!"

        # フォロー機能チェック
        expect_follow_change
      end
    end
  end
end
