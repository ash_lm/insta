require 'rails_helper'

RSpec.describe "Search", type: :system do
  describe "検索機能をチェックする" do
    let(:user) { create(:user, :with_post) }

    def search(key_word)
      sign_in user
      visit root_path
      fill_in "q_content_cont", with: key_word
      click_on "検索"
    end

    context "該当ありの検索" do
      it "正常に検索結果が表示されること" do
        search("テスト")
        aggregate_failures do
          expect(page).to have_title "検索結果・Insta!"
          expect(page).to have_selector("h3", text: "テスト")
          expect(page).to have_selector("p", text: "検索結果 1 件")
          expect(page).to have_selector("img[src$='sample.jpg']")
        end
      end
    end

    context "該当なしの検索" do
      it "条件に該当なしの場合と未入力の場合で表示内容を分岐すること" do
        # 該当なしの場合、「一致なし」と表示
        search("エラー")
        aggregate_failures do
          expect(page).to have_selector("p", text: "検索結果 0 件")
          expect(page).to have_selector("strong", text: "エラー")
        end

        # 未入力の場合、入力を催促する
        search(nil)
        aggregate_failures do
          expect(page).to have_selector("p", text: "キーワードを入力してください。")
        end
      end
    end
  end
end
