require 'rails_helper'

RSpec.describe "Pages", type: :system do
  # ユーザー
  let(:user) { create(:user) }

  # お気に入りユーザー
  let!(:iine_users) do
    create_list(:user, 2) do |u|
      create(:post, :with_image, user: u)
    end
  end
  # フォローユーザー
  let!(:following_user) do
    create(:user) do |u|
      create_list(:post, 2, :with_image, user: u)
    end
  end

  before do
    sign_in user
  end

  describe "ユーザーの利用状況による表示変化をチェックする" do
    context "フォローとお気に入りなし" do
      it "おすすめユーザーのみ表示されていること" do
        visit root_path
        expect(page).to have_title "Insta!"
        expect(page).to have_selector("section.card-header", text: "おすすめリスト")
      end
    end

    context "フォローとお気に入りあり" do
      # 表示に必要なデータ生成
      before do
        iine_users.each { |iine_user| user.iine(iine_user.posts[0]) }
        user.follow(following_user)
      end

      it "フォローユーザーの最新投稿と最新お気に入りが表示されること" do
        posts = user.following.first.posts
        visit root_path

        aggregate_failures do
          # フォロー
          expect(page).to have_content posts.first.content

          # お気に入り
          expect(page).to have_content user.iine_posts.first.content
        end
      end
    end
  end
end
