require 'rails_helper'

RSpec.describe "Like", js: true, type: :system do
  let(:user) { create(:user) }
  let(:other_user) { create(:user, :with_post) }

  before do
    sign_in user
  end

  describe "各ページに配置したお気に入りボタンが作動するかチェックする" do
    context "マイページ" do
      it "正常にお気に入り機能が作動すること" do
        visit user_path(other_user)

        # お気に入り登録
        # 他ユーザー投稿なら削除ボタンの代わりにお気に入りボタンが表示
        find("img[src$='sample.jpg']").click
        expect(page).to have_button "お気に入り"
        expect(page).not_to have_button "投稿を削除する"
        expect do
          click_on "お気に入り"
          expect(page).to have_selector("span", text: "登録済み")
        end.to change(user.likes, :count).by(1)

        # お気に入り解除
        expect do
          click_on "登録済み"
          expect(page).to have_selector("span", text: "お気に入り")
        end.to change(user.likes, :count).by(-1)
      end
    end

    context "投稿個別ページ" do
      it "正常にお気に入り機能が作動すること" do
        visit post_path(other_user.posts[0])

        # お気に入り登録
        expect do
          click_on "お気に入り"
          expect(page).to have_selector("span", text: "登録済み")
        end.to change(user.likes, :count).by(1)
        # お気に入り解除
        expect do
          click_on "登録済み"
          expect(page).to have_selector("span", text: "お気に入り")
        end.to change(user.likes, :count).by(-1)
      end
    end

    context "トップページ" do
      before do
        user.iine(other_user.posts[0])
      end

      it "正常にお気に入り機能が作動すること" do
        visit root_path

        # お気に入り登録
        expect do
          click_on "登録済み"
          expect(page).to have_selector("span", text: "お気に入り")
        end.to change(user.likes, :count).by(-1)
        # お気に入り解除
        expect do
          click_on "お気に入り"
          expect(page).to have_selector("span", text: "登録済み")
        end.to change(user.likes, :count).by(1)
      end
    end
  end
end
