require 'rails_helper'

RSpec.describe "Notification", type: :system do
  let(:user) { create(:user) }
  let(:other_user) { create(:user, :with_post) }

  def expect_post_comment
    fill_in "comment[comment]", with: "あいうえお"
    within ".actions" do
      click_on "投稿する"
    end
    expect(page).to have_selector("p", text: "あいうえお")
  end

  it "各種お知らせ機能のチェック" do
    post = other_user.posts[0]

    sign_in user
    visit post_path(post)
    # 通知機能を発火させる
    aggregate_failures do
      # コメント
      expect do
        expect_post_comment
      end.to change { other_user.passive_notifications.count }.by(1)
      # 10件以上の通知でページネーションを表示させる
      expect_post_comment
      expect_post_comment
      expect_post_comment
      expect_post_comment
      expect_post_comment
      expect_post_comment
      expect_post_comment
      expect_post_comment
      # お気に入り
      expect do
        click_on "お気に入り"
        expect(page).to have_button "登録済み"
        expect(current_path).to eq post_path(post)
      end.to change { other_user.passive_notifications.count }.by(1)
      # フォロー
      expect do
        click_on "フォローする"
        expect(page).to have_button "フォロー中"
      end.to change { other_user.passive_notifications.count }.by(1)
    end

    # ユーザー切り替え
    sign_out user
    sign_in other_user

    # お知らせ表示のチェック
    visit notifications_index_path
    aggregate_failures do
      expect(page).to have_css(".fa-circle")
      expect(page).to have_content("お気に入り登録しました。")
      expect(page).to have_content("フォローしました。")
      expect(page).to have_content("コメントしました。")
      expect(page).to have_css(".pagination")
    end

    # フォローユーザーの新規投稿で通知機能を発火させる
    visit new_post_path
    expect do
      attach_file "post[image]",
                  "#{Rails.root}/spec/files/sample.jpg",
                  make_visible: true
      fill_in "post_content", with: "あいうえお"

      within ".actions" do
        click_on "投稿する"
      end
    end.to change { user.passive_notifications.count }.by(1)

    # # 自分が投稿した画像に対するコメントには通知しない
    visit post_path(other_user.posts.first)
    fill_in "comment[comment]", with: "あいうえお"
    within ".actions" do
      click_on "投稿する"
    end
    sleep 1
    expect(other_user.passive_notifications.first.checked).to be_truthy

    # ユーザー切り替え
    sign_out other_user
    sign_in user

    # お知らせ表示チェック
    visit notifications_index_path
    expect(page).to have_css(".fa-circle")
    expect(page).to have_content("投稿しました。")
    # OKボタンクリックでお知らせを非表示にする
    click_on "OK"
    expect(page).not_to have_css(".fa-circle")
    expect(page).not_to have_content("投稿しました。")
    expect(page).to have_content("通知はありません。")
  end
end
