require "rails_helper"

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  it "ファクトリーが有効であること" do
    expect(user).to be_valid
  end

  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  it { is_expected.to have_many(:posts).dependent(:destroy) }
  it { is_expected.to have_many(:comments).dependent(:destroy) }
  it { is_expected.to have_many(:likes).dependent(:destroy) }
  it { is_expected.to have_many(:iine_posts) }
  it { is_expected.to have_many(:active_relationships).dependent(:destroy) }
  it { is_expected.to have_many(:passive_relationships).dependent(:destroy) }
  it { is_expected.to have_many(:following) }
  it { is_expected.to have_many(:followers) }
  it { is_expected.to have_many(:active_relationships) }
  it { is_expected.to have_many(:passive_relationships) }
  it { is_expected.to have_many(:active_notifications).dependent(:destroy) }
  it { is_expected.to have_many(:passive_notifications).dependent(:destroy) }

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  it { is_expected.to validate_presence_of :fullname }
  it { is_expected.to validate_length_of(:fullname).is_at_most(50) }
  it { is_expected.to validate_presence_of :username }
  it { is_expected.to validate_length_of(:username).is_at_most(50) }
  it { is_expected.to validate_presence_of :password }
  it { is_expected.to validate_length_of(:password).is_at_least(6) }
  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_length_of(:email) }
  # it { is_expected.to validate_uniqueness_of :email } なぜか失敗する...
  # it { is_expected.to validate_uniqueness_of :phone }

  describe "メールアドレスの一意性のチェック" do
    it "重複したメールアドレスなら無効であること" do
      create(:user, email: "pika@example.com")
      other_user = build(:user, email: "pika@example.com")
      other_user.valid?
      expect(other_user.errors[:email]).to include("はすでに存在します")
    end
  end

  describe "メールアドレスのフォーマットのチェック" do
    context "不正な値" do
      it "無効であること" do
        invalid_addresses = %w(user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com foo@bar..com)
        invalid_addresses.each do |invalid_address|
          user.email = invalid_address
          expect(user).not_to be_valid
        end
      end
    end

    context "正常な値" do
      it "有効であること" do
        valid_addresses = %w(user@example.com UseR@foo.Com a_uS-ER@foo.bor.org first.last@foo.jp pika+chui@exam.ple)
        valid_addresses.each do |valid_address|
          user.email = valid_address
          expect(user).to be_valid
        end
      end
    end
  end

  describe "パスワードのフォーマットチェック" do
    context "正常な値" do
      it "有効であること" do
        user.password = "PassWord1"
        user.password_confirmation = "PassWord1"
        expect(user).to be_valid
      end
    end

    context "不正な値" do
      it "無効であること" do
        user.password = "不正なパスワード"
        user.password_confirmation = "不正なパスワード"
        expect(user).not_to be_valid
      end
    end
  end

  describe "携帯番号の一意性のチェック" do
    it "重複した番号なら無効であること" do
      create(:user, phone: "090-1234-5678")
      other_user = build(:user, phone: "090-1234-5678")
      other_user.valid?
      expect(other_user.errors[:phone]).to include("はすでに存在します")
    end
  end

  describe "携帯番号のフォーマットチェック" do
    context "不正な値" do
      it "無効であること" do
        invalid_phones = %w(000-1234-5678 010-1234-5678
          020-1234-5678 030-1234-5678 040-1234-5678 050-1234-5678
          060-1234-5678 ０９０ー１２３４−５６７８ 090-123-4567 080-1234-567
          090-12345-6789 070-1234-56789 09012345678 090--1234-5678 090-1234--5678
        )
        invalid_phones.each do |invalid_phone|
          user.phone = invalid_phone
          expect(user).not_to be_valid
        end
      end
    end

    context "正常な値" do
      it "有効であること" do
        valid_phones = %w(090-1234-5678 080-1234-5678 070-1234-5678)
        valid_phones.each do |valid_phone|
          user.phone = valid_phone
          expect(user).to be_valid
        end
      end
    end
  end

  describe "性別のバリデーションチェック" do
    it "maleまたはfemaleなら受け付けること" do
      valid_genders = %w(male female)
      valid_genders.each do |valid_gender|
        user.gender = valid_gender
        expect(user).to be_valid
      end
    end
  end

  describe "URLのフォーマットチェック" do
    context "不正な値" do
      it "無効であること" do
        invalid_urls = %w(example.com http://e<xample.com
          http://e<xample.com http://e|xample.com http://e"xample.com
          http://e\xample.com http://e^xample.com http://e[xample.com
          http://e`xample.com http://e"xample.com https://ページ
        )
        invalid_urls.each do |invalid_url|
          user.url = invalid_url
          expect(user).not_to be_valid
        end
      end
    end

    context "正常な値" do
      it "有効であること" do
        user.url = "http://Example.com/page1-valid_test"
        expect(user).to be_valid
      end
    end
  end

  describe "Activestorage" do
    subject { user.valid? }

    let!(:avatar) { user.avatar = fixture_file_upload(Rails.root.join("spec", "files", avatar_img)) }

    describe "有効なファイルサイズチェック" do
      context "3MB未満のケース" do
        let(:avatar_img) { "sample_2MB.jpg" }

        it { is_expected.to be_truthy }
      end

      context "3MB以上のケース" do
        let(:avatar_img) { "sample_3MB.jpg" }

        it { is_expected.to be_falsey }
      end
    end

    describe "ファイル形式のチェック" do
      context "有効なファイル" do
        context "jpg" do
          let(:avatar_img) { "sample.jpg" }

          it { is_expected.to be_truthy }
        end

        context "png" do
          let(:avatar_img) { "sample.png" }

          it { is_expected.to be_truthy }
        end
      end

      context "無効なケース" do
        context "gif" do
          let(:avatar_img) { "sample.gif" }

          it { is_expected.to be_falsey }
        end

        context "svg" do
          let(:avatar_img) { "sample.svg" }

          it { is_expected.to be_falsey }
        end

        context "tiff" do
          let(:avatar_img) { "sample.tiff" }

          it { is_expected.to be_falsey }
        end

        context "bmp" do
          let(:avatar_img) { "sample.bmp" }

          it { is_expected.to be_falsey }
        end
      end
    end
  end
end
