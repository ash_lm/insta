require 'rails_helper'

RSpec.describe Notification, type: :model do
  let(:notification) { create(:notification) }

  it "ファクトリーが有効であること" do
    expect(notification).to be_valid
  end

  it { is_expected.to belong_to(:post).optional(true) }
  it { is_expected.to belong_to(:comment).optional(true) }
  it { is_expected.to belong_to(:visitor).optional(true) }
  it { is_expected.to belong_to(:visited).optional(true) }
end
