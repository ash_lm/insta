require 'rails_helper'

RSpec.describe Post, type: :model do
  describe "ファクトリーチェック" do
    let(:post) { create(:post, :with_image) }

    it "ファクトリーが有効であること" do
      expect(post).to be_valid
    end
  end

  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:comments).dependent(:destroy) }
  it { is_expected.to have_many(:likes).dependent(:destroy) }
  it { is_expected.to have_many(:iine_users) }
  it { is_expected.to have_many(:notifications).dependent(:destroy) }

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  it { is_expected.to validate_presence_of :user_id }
  it { is_expected.to validate_presence_of :content }
  it { is_expected.to validate_length_of(:content).is_at_most(250) }
  it { is_expected.to validate_presence_of :image }

  describe "読み出し順序" do
    let!(:years_ago) { create(:post, :with_image, :years_ago) }
    let!(:most_recent) { create(:post, :with_image, :most_recent) }
    let!(:minutes_ago) { create(:post, :with_image, :minutes_ago) }
    let!(:hours_ago) { create(:post, :with_image, :hours_ago) }

    it "降順で取得すること" do
      expect(Post.first).to eq most_recent
      expect(Post.last).to eq years_ago
    end
  end

  describe "Activestorage" do
    def expect_img_valid(img)
      post = Post.new(
        content: "あいうえお",
        image: fixture_file_upload(
          Rails.root.join("spec", "files", img)
        )
      )
      post.valid?
      expect(post.errors[:image].count).to eq 0
    end

    def expect_img_error(img, result)
      post = Post.new(
        content: "あいうえお",
        image: fixture_file_upload(
          Rails.root.join("spec", "files", img)
        )
      )
      post.valid?
      expect(post.errors[:image]).to include(result)
    end

    describe "有効なファイルサイズチェック" do
      context "5MB未満のケース" do
        it "有効であること" do
          expect_img_valid("sample_4MB.jpg")
        end
      end

      context "5MB以上のケース" do
        it "無効であること" do
          expect_img_error("sample_5MB.jpg", "のファイルサイズは5MB未満です。")
        end
      end
    end

    describe "ファイル形式のチェック" do
      context "有効なファイル" do
        it "jpg" do
          expect_img_valid("sample.jpg")
        end

        it "png" do
          expect_img_valid("sample.png")
        end
      end

      context "無効なケース" do
        it "gif" do
          expect_img_error("sample.gif", "のファイル形式が有効ではありません。")
        end

        it "svg" do
          expect_img_error("sample.svg", "のファイル形式が有効ではありません。")
        end

        it "tiff" do
          expect_img_error("sample.tiff", "のファイル形式が有効ではありません。")
        end

        it "bmp" do
          expect_img_error("sample.bmp", "のファイル形式が有効ではありません。")
        end
      end
    end
  end
end
