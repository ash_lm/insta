require 'rails_helper'

RSpec.describe "Comments", type: :request do
  let(:user) { create(:user) }
  # 以下だとwrong number of arguments (given 2, expected 0)になる
  # let(:post) { create(:post, :with_image) }
  let(:post_user) { create(:user, :with_post) }

  describe "#create" do
    context "ログイン時" do
      it "正常にコメントできること" do
        sign_in user
        expect do
          post post_comments_path(post_user.posts[0]),
               params: { comment: { comment: "あいうえお" } }
        end.to change(Comment, :count).by(1)
      end
    end

    context "未ログイン時" do
      it "コメントできずログインページへリダイレクトされること" do
        expect do
          post post_comments_path(post_user.posts[0]),
               params: { comment: { comment: "あいうえお" } }
        end.not_to change(Comment, :count)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
