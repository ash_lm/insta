require 'rails_helper'

RSpec.describe "Relationships", type: :request do
  let(:user) { create(:user) }

  describe "#create" do
    it "未ログインならログインページへリダイレクトすること" do
      expect { post relationships_path }.not_to change(Relationship, :count)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe "#create" do
    it "未ログインならログインページへリダイレクトすること" do
      expect { delete relationship_path(user) }.not_to change(Relationship, :count)
      expect(response).to redirect_to new_user_session_path
    end
  end
end
