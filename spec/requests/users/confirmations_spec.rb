require 'rails_helper'

RSpec.describe "Users::Confirmations", type: :request do
  let(:user) { create(:user, email: "pika@example.com") }

  describe "#new" do
    context "ログイン時" do
      it "マイページへリダイレクトされること" do
        sign_in user
        get new_user_session_path
        expect(response).to redirect_to user
      end
    end

    context "未ログイン時" do
      it "正常にアクセスできること" do
        get new_user_session_path
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "#create" do
    context "アカウント認証済み" do
      it "メール送信されないこと" do
        sign_in user
        expect { confirmation_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    context "アカウント認証前" do
      let(:user) { create(:user, :confirmation) }

      it "正常にメールが再送信されること" do
        post user_registration_path(user)
        expect { confirmation_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)
      end
    end

    context "アカウント作成前" do
      let(:user) { "un_user" }

      it "メール送信されないこと" do
        expect { confirmation_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    describe "メール内容の確認" do
      let(:user) { create(:user, :confirmation) }

      it "正しく表示されていること" do
        confirmation_mail
        mail = ActionMailer::Base.deliveries.last
        aggregate_failures do
          expect(mail.from).to eq ["#{ENV['GMAIL_ADDRESS']}"]
          expect(mail.to).to eq [user.email]
          expect(mail.subject).to eq "メールアドレス確認メール"
          expect(mail.body).to include user.fullname
        end
      end
    end

    def confirmation_mail
      post user_confirmation_path(user), params: { user: { email: "pika@example.com" } }
    end
  end

  describe "#show" do
    it "不正なトークンならアクセスできないこと" do
      get user_confirmation_path("不正なトークン")
      expect(response).to have_http_status(406)
    end
  end
end
