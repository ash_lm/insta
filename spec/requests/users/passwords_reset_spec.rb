require 'rails_helper'

RSpec.describe "Users::Passwords", type: :request do
  let!(:user) { create(:user, email: "pika@example.com") }

  describe "#new" do
    context "ログイン時" do
      it "アクセスできずマイページへリダイレクトされること" do
        sign_in user
        get new_user_password_path
        expect(response).to redirect_to user
      end
    end

    context "未ログイン時" do
      it "正常にアクセスできること" do
        get new_user_password_path
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "#create" do
    context "ログイン時" do
      let(:user) { create(:user) }

      it "メール送信されないこと" do
        sign_in user
        expect { password_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    context "未ログイン時" do
      it "正常にメール送信されること" do
        expect { password_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)
      end
    end

    context "アカウント未作成" do
      let(:user) { "un_user" }

      it "メール送信されないこと" do
        expect { password_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    def password_mail
      post user_password_path(user), params: { user: { email: "pika@example.com" } }
    end
  end

  describe "#edit" do
    it "トークンが不正ならアクセスできないこと" do
      get edit_user_password_path("不正なトークン")
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe "#update" do
    it "トークンが不正なら更新できないこと" do
      put user_password_path("不正なトークン")
      expect(response).to have_http_status(406)
    end
  end

  describe "パスワードをリセットする" do
    before do
      post user_password_path(user), params: { user: { email: user.email } }
    end

    it "passwordメールの内容チェック" do
      mail = ActionMailer::Base.deliveries.last
      aggregate_failures do
        expect(mail.from).to eq ["#{ENV['GMAIL_ADDRESS']}"]
        expect(mail.to).to eq [user.email]
        expect(mail.subject).to eq "パスワードの再設定について"
        expect(mail.body).to include user.fullname
      end
    end
  end
end
