require 'rails_helper'

RSpec.describe "Users::Unlocks", type: :request do
  let(:user) { create(:user, email: "pika@example.com") }

  describe "#new" do
    context "ログイン済み" do
      it "マイページへリダイレクトされること" do
        sign_in user
        get new_user_unlock_path
        expect(response).to redirect_to user
      end
    end

    context "未ログイン" do
      it "正常にアクセスできること" do
        get new_user_unlock_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "#create" do
    context "未凍結ユーザー" do
      it "メール送信されないこと" do
        expect { unlock_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    context "ログイン済み" do
      it "メール送信されないこと" do
        sign_in user
        expect { unlock_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    context "凍結ユーザー" do
      let(:user) { create(:user, :lock) }

      it "メール送信されること" do
        expect { unlock_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(1)
      end
    end

    context "アカウント作成前" do
      let(:user) { "un_user" }

      it "メール送信されないこと" do
        expect { unlock_mail }.to change {
          ActionMailer::Base.deliveries.size
        }.by(0)
      end
    end

    def unlock_mail
      post user_unlock_path(user), params: { user: { email: "pika@example.com" } }
    end
  end

  describe "#show" do
    it "トークンが不正ならアクセスできないこと" do
      get user_unlock_path("不正なトークン")
      expect(response).to have_http_status(406)
    end
  end

  describe "凍結したアカウントを解除する" do
    before do
      error_session
      error_session
      error_session
    end

    def error_session
      post user_session_path(user), params: { user: { email: user.email, password: "違うパスワード" } }
    end

    def extract_unlock_url(mail)
      body = mail.body.encoded
      body[/http[^"]+/]
    end

    it "送信されたメールを通じて凍結を解除すること" do
      expect { error_session }.to change {
        ActionMailer::Base.deliveries.size
      }.by(1)
      mail = ActionMailer::Base.deliveries.last
      url = extract_unlock_url(mail)
      get url
      expect(response).to redirect_to new_user_session_url
      expect(flash[:notice]).to eq "アカウントを凍結解除しました。"
    end

    it "unlockメールの内容チェック" do
      expect { error_session }.to change {
        ActionMailer::Base.deliveries.size
      }.by(1)
      mail = ActionMailer::Base.deliveries.last
      aggregate_failures do
        expect(mail.from).to eq ["#{ENV['GMAIL_ADDRESS']}"]
        expect(mail.to).to eq [user.email]
        expect(mail.subject).to eq "アカウントの凍結解除について"
        expect(mail.body).to include user.fullname
      end
    end
  end
end
