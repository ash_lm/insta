require 'rails_helper'

RSpec.describe "Users::Registrations", type: :request do
  let(:user) { create(:user) }

  def update_user
    { user: {
      profile: "あいうえお",
      phone: "090-1234-5678",
      gender: "male",
      url: "http://example.com",
    } }
  end

  def update_password
    { user: {
      password: "newpassword",
      password_confirmation: "newpassword",
      current_password: "password",
    } }
  end

  describe "#new" do
    it "ログイン時ならマイページにリダイレクトされること" do
      sign_in user
      get new_user_session_path
      expect(response).to redirect_to user
    end
  end

  describe "#create" do
    it "update対象のカラムは受付ないこと" do
      aggregate_failures do
        expect do
          post user_registration_path, params: { user: {
            fullname: "pikachu",
            username: "pika",
            email: "pika@example.com",
            password: "password",
            password_confirmation: "password",
            profile: "あいうえお",
            phone: "090-1234-5678",
            gender: :male,
            url: "https://example.com",
          } }
        end.to change(User, :count).by(1)
        expect(User.first).to have_attributes(
          fullname: "pikachu",
          username: "pika",
          email: "pika@example.com",
        )
        expect(User.first).not_to have_attributes(
          profile: "あいうえお",
          phone: "090-1234-5678",
          gender: "male",
          url: "https://example.com",
        )
        expect(response).to redirect_to root_url
      end
    end
  end

  describe "#edit" do
    describe "ログイン状況によるリダイレクトのチェック" do
      context "ログイン時" do
        it "ユーザーページへリダイレクトすること" do
          sign_in user
          get edit_user_registration_path
          expect(response).to have_http_status(:success)
        end
      end

      context "未ログイン時" do
        it "ログインページへリダイレクトすること" do
          get edit_user_registration_path
          expect(response).to redirect_to new_user_session_url
        end
      end
    end
  end

  describe "#update" do
    describe "ログイン状況によるリダイレクトのチェック" do
      context "ログイン時" do
        it "正常に更新できること" do
          sign_in user
          put user_registration_path, params: update_user
          aggregate_failures do
            expect(user.reload).to have_attributes(
              profile: "あいうえお",
              phone: "090-1234-5678",
              gender: "male",
              url: "http://example.com",
            )
            expect(response).to redirect_to user_path(user)
          end
        end
      end

      context "未ログイン時" do
        it "更新できずログインページへリダイレクトすること" do
          put user_registration_path, params: { user: { fullname: "pikachu" } }
          aggregate_failures do
            expect(user.reload.fullname).not_to eq "pikachu"
            expect(response).to redirect_to new_user_session_url
          end
        end
      end
    end

    describe "editページから更新できるカラムのチェック" do
      it "パスワードの更新はできないこと" do
        sign_in user
        put user_registration_path, params: update_password
        expect(user.reload.password).not_to eq "password1"
      end
    end
  end

  describe "#edit_password" do
    describe "ログイン状況によるリダイレクトのチェック" do
      context "ログイン時" do
        it "正常にアクセスできること" do
          sign_in user
          get users_edit_password_path
          expect(response).to have_http_status(:success)
        end
      end

      context "未ログイン時" do
        it "サインインページへリダイレクトされること" do
          get users_edit_password_path
          expect(response).to redirect_to new_user_session_url
        end
      end
    end
  end

  describe "#update_password" do
    describe "ログイン状況によるリダイレクトのチェック" do
      context "未ログイン時" do
        it "ユーザーページへリダイレクトされること" do
          patch users_update_password_path, params: update_password
          expect(response).to redirect_to new_user_session_url
        end
      end
    end

    describe "edit_passwordページから更新できるカラムのチェック" do
      it "パスワード以外の情報は更新できないこと" do
        sign_in user
        patch users_update_password_path, params: update_user
        expect(user.reload).not_to have_attributes(
          profile: "あいうえお",
          phone: "090-1234-5678",
          gender: "male",
          url: "http://example.com",
        )
      end
    end
  end

  describe "#destroy" do
    describe "ログイン状況によるリダイレクトのチェック" do
      context "ログイン時" do
        it "正常に削除されること" do
          sign_in user
          aggregate_failures do
            expect do
              delete user_registration_path
            end.to change(User, :count).by(-1)
            expect(response).to redirect_to root_url
          end
        end
      end

      context "未ログイン時" do
        it "削除されずサインインページへリダイレクトされること" do
          aggregate_failures do
            expect do
              delete user_registration_path
            end.not_to change(User, :count)
            expect(response).to redirect_to new_user_session_url
          end
        end
      end
    end
  end
end
