require 'rails_helper'

RSpec.describe "Users", type: :request do
  let(:user) { create(:user) }

  describe "#show" do
    context "ログイン時" do
      it "正常にアクセスできること" do
        sign_in user
        get user_path(user)
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "ログインページへリダイレクトされること" do
        get user_path(user)
        expect(response).to redirect_to new_user_session_url
        expect(flash[:danger]).to eq "ログインしてください。"
      end
    end
  end

  describe "#iines" do
    context "ログイン時" do
      it "正常にアクセスできること" do
        sign_in user
        get iines_user_path(user)
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get iines_user_path(user)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#following" do
    context "ログイン時" do
      it "正常にアクセスできること" do
        sign_in user
        get following_user_path(user)
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get following_user_path(user)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#followers" do
    context "ログイン時" do
      it "正常にアクセスできること" do
        sign_in user
        get followers_user_path(user)
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get followers_user_path(user)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
