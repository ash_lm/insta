require 'rails_helper'

RSpec.describe "Notifications", type: :request do
  let(:user) { create(:user) }
  let(:notification) { create(:notification) }

  describe "#index" do
    context "ログイン時" do
      it "正常にアクセスできること" do
        sign_in user
        get notifications_path
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get notifications_path
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#update" do
    context "ログイン時" do
      it "未ログインならログインページへリダイレクトすること" do
        patch notification_path(notification)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
