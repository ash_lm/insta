require 'rails_helper'

RSpec.describe "Likes", type: :request do
  let!(:like_user) { create(:user, :with_post) }

  describe "#create" do
    it "未ログインならログインページへリダイレクトされること" do
      expect { post likes_path }.not_to change(Like, :count)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe "#destroy" do
    it "未ログインならログインページへリダイレクトされること" do
      expect { delete like_path(like_user.posts[0]) }.not_to change(Like, :count)
      expect(response).to redirect_to new_user_session_path
    end
  end
end
