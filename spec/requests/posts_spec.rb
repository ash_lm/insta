require 'rails_helper'

RSpec.describe "Posts", type: :request do
  let!(:user) { create(:user, :with_post) }

  describe "#index" do
    context "ログイン時" do
      it "アクセスできること" do
        sign_in user
        get posts_path
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get posts_path
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#new" do
    context "ログイン時" do
      it "アクセスできること" do
        sign_in user
        get new_post_path
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get new_post_path
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#create" do
    context "ログイン時" do
      it "登録できること" do
        sign_in user
        expect do
          post posts_path, params: { post: {
            image: fixture_file_upload(Rails.root.join("spec", "files", "sample.jpg")),
            content: "あいうえお",
          } }
        end.to change(Post, :count).by(1)
      end
    end

    context "未ログイン時" do
      it "登録できずログインページへリダイレクトされること" do
        expect do
          post posts_path, params: { post: {
            image: fixture_file_upload(Rails.root.join("spec", "files", "sample.jpg")),
            content: "あいうえお",
          } }
        end.not_to change(Post, :count)
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#show" do
    context "ログイン時" do
      it "アクセスできること" do
        sign_in user
        get post_path(user.posts[0])
        expect(response).to have_http_status(:success)
      end
    end

    context "未ログイン時" do
      it "アクセスできずログインページへリダイレクトされること" do
        get post_path(user.posts[0])
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe "#destroy" do
    context "ログイン時" do
      it "正常に削除できること" do
        sign_in user
        expect { delete post_path(user.posts[0]) }.to change(Post, :count).by(-1)
      end
    end

    context "未ログイン時" do
      it "削除できずログインページへリダイレクトされること" do
        expect { delete post_path(user.posts[0]) }.not_to change(Post, :count)
      end
    end

    context "他ユーザーの投稿" do
      let(:other_user) { create(:user) }

      it "削除できないこと" do
        sign_in other_user
        expect { delete post_path(user.posts[0]) }.not_to change(Post, :count)
      end
    end
  end
end
