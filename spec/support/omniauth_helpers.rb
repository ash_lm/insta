module OmniAuthHelpers
  def set_omniauth(service = :facebook)
    OmniAuth.config.test_mode = true

    OmniAuth.config.mock_auth[service] = OmniAuth::AuthHash.new({
      provider: service.to_s,
      uid: "1234",
      info: {
        fullname: "pikachu",
        username: "pika",
        email: "pika@example.com",
        password: "password",
      },
    })
  end
end
