FactoryBot.define do
  factory :notification do
    visitor_id { 1 }
    visited_id { 2 }
    post { nil }
    comment { nil }
    action { "post" }
    checked { false }
  end
end
