FactoryBot.define do
  factory :comment, class: "Comment" do
    association :post
    user { post.user }
    comment { "あいうえお" }
  end
end
