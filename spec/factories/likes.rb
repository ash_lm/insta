FactoryBot.define do
  factory :like, class: "Like" do
    association :post
    user { post.user }
  end
end
