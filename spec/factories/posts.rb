FactoryBot.define do
  factory :post, class: "Post" do
    sequence(:content) { |n| "テストコメント-#{n}" }
    association :user
    # after処理でないと作動しなかったのでtrait処理にする
    trait :with_image do
      after(:build) do |post|
        post.image.attach(io: File.open(
          Rails.root.join('spec', 'files', "sample.jpg")
        ),filename: 'sample.jpg', content_type: 'image/jpeg')
      end
    end

    trait :most_recent do
      created_at { Time.zone.now }
    end

    trait :minutes_ago do
      created_at { 1.minutes.ago }
    end

    trait :hours_ago do
      created_at { 1.hours.ago }
    end

    trait :years_ago do
      created_at { 1.years.ago }
    end

    factory :post_order, traits: [:most_recent, :minutes_ago, :hours_ago, :years_ago]
  end
end
