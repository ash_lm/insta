FactoryBot.define do
  factory :user, class: "User" do
    sequence(:fullname) { |n| "pikachu-#{n}" }
    sequence(:username) { |n| "pika-#{n}" }
    sequence(:email) { |n| "pika#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
    confirmed_at { Time.zone.now }

    trait :confirmation do
      email { "pika@example.com" }
      confirmed_at { nil }
    end

    trait :lock do
      email { "pika@example.com" }
      confirmed_at { Time.zone.now }
      locked_at { Time.zone.now }
      unlock_token { SecureRandom.urlsafe_base64 }
    end

    trait :facebook do
      provider { "facebook" }
      uid { "1234" }
    end

    trait :with_post do
      after(:create) { |u| create(:post, :with_image, user: u) }
    end
  end
end
