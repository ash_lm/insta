class AddUsernameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :fullname, :string, null: false, limit: 50
    add_column :users, :username, :string, null: false, limit: 50
  end
end
