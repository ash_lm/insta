class AddUserEditColumnsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :profile, :text
    add_column :users, :phone, :string
    add_column :users, :gender, :integer
  end
end
