# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ユーザー
User.create!(
  fullname: 'pikachu',
  username: 'pika',
  email: 'pika@example.com',
  password: "password",
  password_confirmation: "password",
  # ランダムな４桁の整数
  phone: "090-#{sprintf("%.4d", rand(10000))}-#{sprintf("%.4d", rand(10000))}",
  url: "http://example.com",
  profile: 'マサラタウン',
  confirmed_at: Time.zone.now,
)

19.times do |n|
  fullname = Faker::Games::Zelda.game
  username = Faker::Games::Zelda.character
  email = "pika#{n+1}@example.com"
  User.create!(
    fullname: fullname,
    username: username,
    email: email,
    password: "password",
    password_confirmation: "password",
    # ランダムな４桁の整数
    phone: "090-#{sprintf("%.4d", rand(10000))}-#{sprintf("%.4d", rand(10000))}",
    url: "http://example.com",
    profile: Faker::Lorem.sentence(word_count: 6),
    confirmed_at: Time.zone.now,
    )
  end

# 投稿
users = User.order(:created_at).take(6)
20.times do
  content = Faker::Lorem.sentence(word_count: 6)
  users.each { |user|
  post = user.posts.build(content: content)
  post.image.attach(
    io: File.open(Rails.root.join("db/fixtures/sample.jpg")),filename: "sample.jpg")
  post.save!
  }
end

# フォロー
users = User.all.order(created_at: :asc)
user = users.first
following = users[1..11]
followers = users[2..15]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

# お気に入り
users = User.all.order(created_at: :asc)
user = users.first
post_users = users[2..5]
post_users.each { |post_user| user.iine(post_user.posts.first) }
iine_users = users[3..12]
iine_users.each { |iine_user| iine_user.iine(user.posts.first) }
