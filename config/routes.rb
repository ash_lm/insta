Rails.application.routes.draw do
  get 'notifications/index'
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks', registrations: 'users/registrations',
    sessions: 'users/sessions', confirmations: 'users/confirmations', passwords: 'users/passwords',
    unlocks: 'users/unlocks',
  }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_scope :user do
    get 'users/edit_password', to: 'users/registrations#edit_password'
    patch 'users/update_password', to: 'users/registrations#update_password'
    post 'users/sign_up/confirm', to: 'users/registrations#confirm_new'
  end

  controller :pages do
    root 'pages#home'
    get '/term', to: 'pages#term'
  end

  controller :users do
    resources :users, only: [:show] do
      member do
        get :following, :followers, :iines
      end
    end
  end

  controller :posts do
    resources :posts, only: [:index, :new, :create, :show, :destroy] do
      resources :comments, only: [:create]
    end
  end

  controller :likes do
    resources :likes, only: [:create, :destroy]
  end

  controller :relationships do
    resources :relationships, only: [:create, :destroy]
  end

  controller :notifications do
    resources :notifications, only: [:index, :update]
  end
end
