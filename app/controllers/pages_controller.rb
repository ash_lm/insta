class PagesController < ApplicationController
  include PagesHelper

  def home
    if user_signed_in?
      # フォロー関連のインスタンス変数 一番新しいフォローを呼び出す
      unless current_user.following_feed.count == 0
        @follow_post = current_user.following_feed.first
        @follow_user = @follow_post.user
        @follow_comments = @follow_post.comments.includes(
          user: [avatar_attachment: :blob])
      end

      # お気に入り関連のインスタンス変数 一番新しいお気に入り呼び出す
      unless current_user.likes.count == 0
        @iine_post = current_user.likes.first.post
        @iine_user = @iine_post.user
        @iine_comments = @iine_post.comments.includes(
          user: [avatar_attachment: :blob])
      end

      # おすすめユーザーのインスタンス変数 フォローしていないユーザーをランダムに呼び出す
      @recommends = User.all.where.not(id: current_user.following.ids
        ).where.not(id: current_user.id).includes([avatar_attachment: :blob]).sample(12)
    end
  end

  def term
  end
end
