class UsersController < ApplicationController
  before_action :sign_in_required
  before_action :setup_user

  def show
    @list_name = "投稿リスト #{@user.posts_count} 件"
    @title = @user.username
    @posts = @user.posts.paginate(page: params[:page], per_page: 9).includes(
      [image_attachment: [:blob]],
      comments: [user: [avatar_attachment: [:blob]]]
    )
  end

  def iines
    @title = "お気に入り"
    @list_name = "お気に入りリスト #{@user.likes_count} 件"
    @posts = @user.iine_posts.paginate(page: params[:page], per_page: 9).includes(
      [image_attachment: [:blob]],
      [user: [avatar_attachment: [:blob]]],
      comments: [user: [avatar_attachment: [:blob]]]
    )
    render "show"
  end

  def following
    @title = "フォロー"
    @users = @user.following.paginate(page: params[:page], per_page: 10).includes(avatar_attachment: [:blob])
    render "show_follow"
  end

  def followers
    @title = "フォロワー"
    @users = @user.followers.paginate(page: params[:page], per_page: 10).includes(avatar_attachment: [:blob])
    render "show_follow"
  end

  private

  def setup_user
    @user = User.find(params[:id])
  end
end
