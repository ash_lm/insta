class ApplicationController < ActionController::Base
  include ApplicationHelper

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_search

  # サインイン後のリダイレクト先変更
  def after_sign_in_path_for(resource)
    user_path resource
  end

  # ヘッダーからどこからでも検索できるようにする
  def set_search
    @search = Post.ransack(params[:q])
    if params[:q] && params[:q].reject { |key, value| value.blank? }.present?
      @search = Post.ransack(search_params)
      @search_posts = @search.result.paginate(page: params[:page], per_page: 9).includes(
        [user: [avatar_attachment: :blob]],
        [image_attachment: [:blob]],
        [comments: [user: [avatar_attachment: :blob]]]
      )
    end
  end

  protected

  # 追加のUser属性
  def configure_permitted_parameters
    added_attrs = [:fullname, :username]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
  end

  private

  # ログインユーザーのチェック
  def sign_in_required
    unless user_signed_in?
      flash[:danger] = "ログインしてください。"
      redirect_to new_user_session_url
    end
  end

  def search_params
    params.require(:q).permit(:content_cont)
  end
end
