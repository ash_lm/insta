class LikesController < ApplicationController
  before_action :sign_in_required

  def create
    @post = Post.find(params[:post_id])
    unless current_user.iine?(@post)
      current_user.iine(@post)
      @post.create_notification_like(current_user)
      respond_to do |format|
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      end
    end
  end

  def destroy
    @post = Like.find(params[:id]).post
    if current_user.iine?(@post)
      current_user.uniine(@post)
      respond_to do |format|
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      end
    end
  end
end
