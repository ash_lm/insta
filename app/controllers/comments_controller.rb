class CommentsController < ApplicationController
  before_action :sign_in_required

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    respond_to do |format|
      if @comment.save
        @post.save_notification_comment(current_user, @comment.id, @post.user.id)
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      else
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:comment).merge(user_id: current_user.id)
  end
end
