class RelationshipsController < ApplicationController
  before_action :sign_in_required

  def create
    @user = User.find(params[:followed_id])
    unless current_user.following?(@user)
      current_user.follow(@user)
      @user.create_notification_follow(current_user)
      respond_to do |format|
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      end
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    if current_user.following?(@user)
      current_user.unfollow(@user)
      respond_to do |format|
        format.html { redirect_to request.referrer || user_url(current_user) }
        format.js
      end
    end
  end
end
