class NotificationsController < ApplicationController
  before_action :sign_in_required

  def index
    # 現在のユーザーとチェック済み以外を取得
    @notifications = current_user.passive_notifications.
      paginate(page: params[:page], per_page: 10).
      where(checked: false).
      where.not(visitor_id: current_user.id).
      includes(
        [:post],
        [:visitor, :visited],
        [visitor: [:avatar_attachment]]
      )

    # 未確認を済み(true)に更新する
    # @notifications.where(checked: false).each do |notification|
    #   notification.update_attributes(checked: true)
    # end
  end

  def update
    @notification = Notification.find(params[:id])
    @notification.update_attributes(checked: true)
    redirect_to request.referrer || user_url(current_user)
  end
end
