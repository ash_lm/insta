class PostsController < ApplicationController
  before_action :sign_in_required
  before_action :correct_user, only: :destroy

  def index
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    @post.image.attach(params[:post][:image])
    if @post.save
      @post.create_notification_post(current_user)
      flash[:notice] = "投稿が完了しました。"
      redirect_to user_url(current_user)
    else
      render "new"
    end
  end

  def show
    @post = Post.find(params[:id])
    @user = @post.user
    @comment = @post.comments.build
    @comments = @post.comments.includes(
      [:user],
      [user: [avatar_attachment: [:blob]]]
    )
  end

  def destroy
    @post.destroy
    flash[:success] = "投稿を削除しました。"
    redirect_to user_url(current_user)
  end

  private

  def post_params
    params.require(:post).permit(:content, :image)
  end

  # ユーザー確認
  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to current_user if @post.nil?
  end
end
