class User < ApplicationRecord
  # Include default devise modules. Others available are:
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :validatable, :confirmable, :lockable, :timeoutable,
         :trackable, :omniauthable, omniauth_providers: [:facebook]

  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  # アバター
  has_one_attached :avatar, dependent: :destroy

  # 投稿
  has_many :posts, dependent: :destroy

  # コメント
  has_many :comments, dependent: :destroy

  # いいね
  has_many :likes, dependent: :destroy
  # いいねした投稿を配列の様に集める
  has_many :iine_posts, through: :likes, source: :post

  # フォロー
  has_many :active_relationships,
           class_name: "Relationship",
           foreign_key: "follower_id",
           dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed

  # フォロワー
  has_many :passive_relationships,
           class_name: "Relationship",
           foreign_key: "followed_id",
           dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower

  # 通知機能
  # 通知を送った側
  has_many :active_notifications,
           class_name: "Notification",
           foreign_key: "visitor_id",
           dependent: :destroy

  # 通知を受け取った側
  has_many :passive_notifications,
           class_name: "Notification",
           foreign_key: "visited_id",
           dependent: :destroy

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  # 氏名
  validates :fullname, presence: true, length: { maximum: 50 }

  # ユーザー名
  validates :username, presence: true, length: { maximum: 50 }

  # パスワード
  VALID_PASSWORD_REGEX = /\A^[0-9a-zA-Z]*$\Z/.freeze
  validates :password, format: { with: VALID_PASSWORD_REGEX }

  # メールアドレス
  # メールアドレスの正規表現が設定されていないようなのでチュートリアルを転用
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze
  validates :email, format: { with: VALID_EMAIL_REGEX }

  # 携帯番号
  VALID_PHONE_REGEX = /\A^0[789]0-[0-9]{4}-[0-9]{4}$\z/.freeze
  validates :phone, uniqueness: true, format: { with: VALID_PHONE_REGEX }, allow_blank: true

  # 性別
  # 指定値以外はArgumentErrorが出る
  enum gender: { male: 0, female: 1 }
  # テストが上手くいかなかった。上記の扱いで問題はないか？
  # validates :gender, inclusion: {in: [male: 0, female: 1]}, allow_blank: true

  # URL
  validates :url, format: /\A#{URI.regexp(%w(http https))}\z/, allow_blank: true

  # アバター
  validates :avatar,
            content_type: {
              in: %w(image/jpeg image/png),
              message: "のファイル形式が有効ではありません。。",
            },
            size: {
              less_than: 3.megabytes, message: "のファイルサイズは5MB未満です。",
            }

  # メソッドーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  # 表示用にリサイズした画像を返す
  def display_avatar
    avatar.variant(resize_to_limit: [150, 150])
  end

  # 投稿にいいねをする
  def iine(post)
    iine_posts << post
  end

  # 投稿のいいねを解除する
  def uniine(post)
    likes.find_by(post_id: post.id).destroy
  end

  # いいねの確認
  def iine?(post)
    iine_posts.include?(post)
  end

  # フォロー中？
  def following?(other_user)
    following.include?(other_user)
  end

  # フォローする
  def follow(other_user)
    following << other_user
  end

  # フォロー解除
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # フォロー通知の作成
  def create_notification_follow(current_user)
    temp = Notification.where([
      "visitor_id = ? and visited_id = ? and action = ? ",
      current_user.id, id, "follow",
    ])
    if temp.blank?
      notification = current_user.active_notifications.new(
        visited_id: id,
        action: "follow"
      )
      notification.save if notification.valid?
    end
  end

  # フォローフィード フォローユーザーの投稿を集める
  def following_feed
    following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
    Post.where("user_id IN (#{following_ids})", user_id: id)
  end

  # omniauth
  # providerとuidで認証、無ければブロック内の内容を登録
  def self.from_omniauth(auth)
    find_or_create_by(provider: auth["provider"], uid: auth["uid"]) do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.fullname = auth["info"]["fullname"]
      user.username = auth["info"]["username"]
      user.email = auth["info"]["email"]
      user.password = auth["info"]["password"]
    end
  end

  # Facebookから受け取ったproviderとuidを登録
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
      end
    else
      super
    end
  end
end
