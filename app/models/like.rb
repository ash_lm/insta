class Like < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  counter_culture :user
  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  belongs_to :user
  belongs_to :post

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  validates :user_id, presence: true
  validates :post_id, presence: true
end
