class Comment < ApplicationRecord
  default_scope -> { order(created_at: :desc) }

  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  belongs_to :user
  belongs_to :post

  # 通知
  has_many :notifications, dependent: :destroy

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  validates :comment, presence: true, length: { maximum: 50 }
end
