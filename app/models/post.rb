class Post < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  counter_culture :user
  # リレーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  # ユーザー
  belongs_to :user

  # 画像
  has_one_attached :image, dependent: :destroy

  # コメント
  has_many :comments, dependent: :destroy

  # いいね
  has_many :likes, dependent: :destroy
  has_many :iine_users, through: :likes, source: :user

  # 通知
  has_many :notifications, dependent: :destroy

  # バリデーションーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  validates :user_id, presence: true

  # 内容
  validates :content, presence: true, length: { maximum: 250 }

  # 画像
  validates :image,
            presence: true,
            content_type: {
              in: %w(image/jpeg image/png),
              message: "のファイル形式が有効ではありません。",
            },
            size: {
              less_than: 5.megabytes, message: "のファイルサイズは5MB未満です。",
            }

  # メソッドーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  # 表示用にリサイズした画像を返す
  def display_image
    image.variant(resize_to_fill: [600, 600]).processed
  end

  # 通知機能 CommentとLikeの親モデルであるPostに作成
  # お気に入り通知の作成
  def create_notification_like(current_user)
    # 対象を検索
    temp = Notification.where([
      "visitor_id = ? and visited_id = ? and post_id = ? and action = ? ",
      current_user.id, user_id, id, "like",
    ])
    # 対象が存在しなければ、通知を作成
    if temp.blank?
      notification = current_user.active_notifications.new(
        visited_id: user_id,
        post_id: id,
        action: "like"
      )
      # 自分の投稿に対しては通知済みと処理する
      if notification.visitor_id == notification.visited_id
        notification.checked = true
      end
      notification.save if notification.valid?
    end
  end

  # コメント通知の作成
  # コメントは同じ投稿に複数できるので、複数回呼び出せるようにする
  def save_notification_comment(current_user, comment_id, visited_id)
    # コメントの有無による条件分岐はなし
    notification = current_user.active_notifications.new(
      visited_id: visited_id,
      post_id: id,
      comment_id: comment_id,
      action: "comment"
    )
    # 自分の投稿に対しては通知済みと処理する
    if notification.visitor_id == notification.visited_id
      notification.checked = true
    end
    notification.save if notification.valid?
  end

  def create_notification_post(current_user)
    followers = current_user.followers.ids
    followers.each do |follower|
      save_notification_post(current_user, follower)
    end
  end

  def save_notification_post(current_user, visited_id)
    notification = current_user.active_notifications.new(
      post_id: id,
      visited_id: visited_id,
      action: "post"
    )
    notification.save if notification.valid?
  end
end
