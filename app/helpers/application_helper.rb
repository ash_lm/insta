module ApplicationHelper
  def full_title(title = "")
    base_title = "Insta!"
    if title.present?
      "#{title}・#{base_title}"
    else
      base_title
    end
  end

  def current_user?(user)
    user&. == current_user
  end
end
