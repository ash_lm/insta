$(document).on("turbolinks:load", function () {
  $("#user_avatar").bind("change", function () {
    var size_in_megabytes = this.files[0].size / 1024 / 1024;
    var ImgSrc = "https://placehold.jp/150x150.png?text=Non-Image";
    if (size_in_megabytes > 3) {
      alert(
        "最大ファイルサイズは３MB未満です。\nこれより小さいファイルを選択してください。"
      );
      $("#user_avatar").val("");
      $(".prev-image").attr({ src: ImgSrc });
    }
  });

  $(function () {
    // 画像をプレビュー表示させる.prev-contentを作成
    function buildHTML(image) {
      var html = `
          <div class="prev-content">
            <img src="${image}", alt="preview" class="prev-image" width=150 height=150>
          </div>
          `;
      return html;
    }

    $(document).on("change", ".hidden_file", function () {
      var file = this.files[0];
      var reader = new FileReader();
      // DataURIScheme文字列を取得
      reader.readAsDataURL(file);
      // 読み込みが完了したら処理が実行
      reader.onload = function () {
        // 読み込んだファイルの内容を取得して変数imageに代入
        var image = this.result;
        // プレビュー画像がなければ処理を実行
        if ($(".prev-content").length == 0) {
          // 読み込んだ画像ファイルをbuildHTMLに渡す
          var html = buildHTML(image);
          // 作成した.prev-contentをiconの代わりに表示
          $(".prev-contents").prepend(html);
          // 画像が表示されるのでiconを隠す
          $(".photo-icon").hide();
        } else {
          // もし既に画像がプレビューされていれば画像データのみを入れ替える
          $(".prev-content .prev-image").attr({ src: image });
        }
      };
    });
  });
});
