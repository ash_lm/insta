$(document).on("turbolinks:load", function () {
  $("#post_img").bind("change", function () {
    var size_in_megabytes = this.files[0].size / 1024 / 1024;
    var ImgSrc = "https://placehold.jp/618x618.png?text=画像を選択";
    if (size_in_megabytes > 5) {
      alert(
        "最大ファイルサイズは5MB未満です。\nこれより小さいファイルを選択してください。"
      );
      $("#post_img").val("");
      $("#img_prev").attr({ src: ImgSrc });
    }
  });

  $(function () {
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $("#img_prev").attr("src", e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#post_img").change(function () {
      readURL(this);
    });
  });
});
