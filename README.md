# ポテパンキャンプ サブ課題

これは、「ポテパンキャンプ」課題の一環で作成したインスタグラムのサンプルです。

## アプリの URL

### \*\*https://sample-insta.herokuapp.com/

## 使い方

開発環境で、このアプリケーションを動かす場合は、まずはリポジトリを手元にクローンしてください。

`$ git clone git@bitbucket.org:ash_lm/sample-insta.git`

事前に postgresql をインストールしておいてください。

その後、次のコマンドで必要になる RubyGems をインストールします。

```
$ bundle
```

その後、データベースを作成してマイグレーションを実行します。

```
$ bin/rails db:create
$ bin/rails db:migrate
$ bin/rails db:seed
```

最後に、テストを実行してうまく動いているかどうか確認してください。
カバレッジへ反映するために、一度 spring を停止してから実行してください。

```
$ bin/spring stop
$ rspe spec
```

「Notification 各種お知らせ機能のチェック」でエラーが発生するケースがありますが、
アプリは問題なく動作します。

テストが無事に通ったら、Rails サーバーを立ち上げる準備が整っているはずです。

```
$ bin/rails s
```

https://localhost:3000
